﻿using Texo.Teste.Domain.Entities;
using Texo.Teste.Repository.Interface.Interfaces;

namespace Texo.Teste.Repository.Interface
{
    public interface IUnitOfWork
    {
        IBaseRepository<TEntity> RetornaRepositorioDe<TEntity>() where TEntity : BaseEntity;
    }
}
