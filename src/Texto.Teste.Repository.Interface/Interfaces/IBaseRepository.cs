﻿using System.Collections.Generic;
using Texo.Teste.Domain.Entities;

namespace Texo.Teste.Repository.Interface.Interfaces
{
    public interface IBaseRepository<TEntity> where TEntity : BaseEntity
    {
        bool Exclui(TEntity entidade);
        IReadOnlyList<TEntity> PesquisaPor(IQuerySpecification<TEntity> especificacao);
        IReadOnlyList<TEntity> RetornaTodosRegistros();
    }
}
