﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Texo.Teste.Repository.Interface.Interfaces
{
    public interface IQuerySpecification<T>
    {
        Expression<Func<T, bool>> Criterio { get; }
        List<Expression<Func<T, object>>> Includes { get; }
    }
}
