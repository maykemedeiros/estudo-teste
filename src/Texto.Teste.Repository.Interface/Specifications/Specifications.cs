﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Texo.Teste.Repository.Interface.Interfaces;

namespace Texo.Teste.Repository.Interface.Specifications
{
    public abstract class QuerySpecification<T> : IQuerySpecification<T>
    {
        public QuerySpecification(Expression<Func<T, bool>> criterio) =>
            Criterio = criterio;

        public Expression<Func<T, bool>> Criterio { get; }

        public List<Expression<Func<T, object>>> Includes { get; } = new List<Expression<Func<T, object>>>();

        protected virtual void AdicionarInclude(Expression<Func<T, object>> include) =>
            Includes.Add(include);
    }
}
