﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Swashbuckle.AspNetCore.Swagger;
using Texo.Teste.IoC;
using Texo.Teste.Repository;
using Texo.Teste.Repository.Contexts;
using Texo.Teste.Service.Interface.Settings;

namespace Texo.Teste.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            InjetorDependencia.Register(services);

            services.AddCors(o =>
            {
                o.AddPolicy("default", builder =>
                    builder.AllowAnyOrigin()
                        .AllowAnyMethod()
                        .AllowAnyHeader()
                        .AllowCredentials());
            });

            services.AddDbContext<APIContext>(o =>
                o.UseInMemoryDatabase("APIDatabase"));

            services.AddMvc();

            services.AddSwaggerGen(o =>
            {
                o.SwaggerDoc("v1", new Info
                {
                    Title = "Texo.Teste.API",
                    Version = "v1"
                });
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, APIContext context)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            var path = Configuration.GetValue<string>("DbSeedPath:Path");

            app.UseCors("default");

            app.UseSwagger();
            app.UseSwaggerUI(o =>
            {
                o.SwaggerEndpoint("/swagger/v1/swagger.json", "Texo.Teste.API v1");
            });

            app.UseMvc();

            DbInitializer.Seed(context, path);
        }
    }
}
