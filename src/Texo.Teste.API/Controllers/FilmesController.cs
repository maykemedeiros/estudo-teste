﻿using Microsoft.AspNetCore.Mvc;
using Texo.Teste.Service.Interface.Services;

namespace Texo.Teste.API.Controllers
{
    [Route("api/piorfilme/filmes")]
    [ApiController]
    public class FilmesController : Controller
    {
        private readonly IIndicadosService indicadosService;

        public FilmesController(IIndicadosService indicadosService)
        {
            this.indicadosService = indicadosService;
        }

        [HttpDelete]
        [Route("excluir/{nomeFilme}")]
        public IActionResult ExcluirFilme(string nomeFilme)
        {
            var excluiu = indicadosService.Exclui(nomeFilme);

            if (excluiu)
            {
                return Ok("Filme excluído com sucesso.");
            }

            return BadRequest("Houve um problema na exclusão do filme. O filme não existe ou é um vencedor do troféu.");
        }
    }
}