﻿using Microsoft.AspNetCore.Mvc;
using Texo.Teste.Domain.Extensions;
using Texo.Teste.Service.Interface.Services;

namespace Texo.Teste.API.Controllers
{
    [Produces("application/json")]
    [Route("api/piorfilme/vencedores")]
    [ApiController]
    public class VencedoresPiorFilmeController : Controller
    {
        private readonly IIndicadosService indicadosService;

        public VencedoresPiorFilmeController(IIndicadosService indicadosService)
        {
            this.indicadosService = indicadosService;
        }

        [HttpGet]
        [Route("ano/{ano}")]
        public IActionResult GetVencedoresEm(string ano)
        {
            if (ano.OnlyNumbers() && ano.Length == 4)
            {
                var vencedores = indicadosService.ObterVencedoresEm(ano);

                if (vencedores.Count > 0)
                {
                    return Ok(vencedores);
                }

                return Ok("Não foram encontrados resultados para o ano informado.");
            }

            return BadRequest("Apenas números com 4 caracteres são permitidos no ano.");
;       }

        [HttpGet]
        [Route("ano/anos_com_mais_de_um")]
        public IActionResult AnosComMaisDeUmVencedor()
        {
            var anos = indicadosService.ObterAnosComMaisDeUmVencedor();

            return Ok(anos);
        }

        [HttpGet]
        [Route("estudios/ordenado_por_premiacoes")]
        public IActionResult EstudioOrdenadoPorQtdPremiacoes()
        {
            var estudiosOrdenados = indicadosService.ObterEstudiosOrdenadoQtdPremio();

            return Ok(estudiosOrdenados);
        }

        [HttpGet]
        [Route("produtor/maior_menor_intervalo_premios")]
        public IActionResult GetProdutorMaiorEMenorIntervalosPremios()
        {
            var produtores = indicadosService.GetProdutorMaiorIntervalosPremiosDoisPremiosMaisRápido();

            return Ok(produtores);
        }
    }
}