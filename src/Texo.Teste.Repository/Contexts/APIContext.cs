﻿using Microsoft.EntityFrameworkCore;
using Texo.Teste.Domain.Entities;

namespace Texo.Teste.Repository.Contexts
{
    public class APIContext : DbContext
    {
        public APIContext(DbContextOptions<APIContext> options) 
            : base(options)
        {
        }

        public DbSet<IndicadoPiorFilme> IndicadosPiorFilme { get; set; }
    }
}
