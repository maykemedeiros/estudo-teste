﻿using System;
using System.Collections.Generic;
using Texo.Teste.Domain.Entities;
using Texo.Teste.Repository.Contexts;
using Texo.Teste.Repository.Repositories;
using Texo.Teste.Repository.Interface.Interfaces;
using Texo.Teste.Repository.Interface;

namespace Texo.Teste.Repository
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly APIContext context;
        private readonly Dictionary<Type, object> repositorios;

        public UnitOfWork(APIContext context)
        {
            this.context = context;
            repositorios = new Dictionary<Type, object>();
        }

        public IBaseRepository<TEntity> RetornaRepositorioDe<TEntity>() where TEntity : BaseEntity
        {
            var type = typeof(TEntity);
            if (!repositorios.ContainsKey(type))
            {
                repositorios.Add(typeof(TEntity),
                    new BaseRepository<TEntity>(context));
            }
            return repositorios[type] as IBaseRepository<TEntity>;
        }
    }
}
