﻿using System;
using System.IO;
using System.Linq;
using Texo.Teste.Domain.Entities;
using Texo.Teste.Repository.Contexts;

namespace Texo.Teste.Repository
{
    public static class DbInitializer
    {
        public static void Seed(APIContext context, string path)
        {
            var dadosCSV = File.ReadAllLines(@path)
                .Select(linha => linha.Split(";"))
                .Skip(1)
                .Select(linha => new IndicadoPiorFilme()
                {
                    Ano = linha[0],
                    Titulo = linha[1],
                    Estudios = linha[2].Split(", ").ToList()
                        .Select(e => new Estudio() { Nome = e }).ToList(),
                    Produtores = linha[3].Split(" and ").ToList()
                        .Select(p => new Produtor() { Nome = p }).ToList(),
                    Venceu = linha[4].Equals("yes", StringComparison.InvariantCultureIgnoreCase) 
                        ? true : false
                })
                .ToList();

            context.IndicadosPiorFilme.AddRange(dadosCSV);
            context.SaveChanges();
        }
    }
}
