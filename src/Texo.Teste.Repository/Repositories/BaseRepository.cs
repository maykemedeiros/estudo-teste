﻿using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using Texo.Teste.Domain.Entities;
using Texo.Teste.Repository.Contexts;
using Texo.Teste.Repository.Interface.Interfaces;

namespace Texo.Teste.Repository.Repositories
{
    public class BaseRepository<TEntity> : IBaseRepository<TEntity> where TEntity : BaseEntity
    {
        protected APIContext context;

        public BaseRepository(APIContext context) : base() =>
            this.context = context;
               

        public bool Exclui(TEntity entidade)
        {
            context.Remove(entidade);
            return context.SaveChanges() > 0;
        }

        public IReadOnlyList<TEntity> PesquisaPor(IQuerySpecification<TEntity> especificacao)
        {
            var queryable = especificacao
                .Includes.Aggregate(context.Set<TEntity>().AsQueryable(),
                    (atual, include) => atual.Include(include));

            return queryable.Where(especificacao.Criterio).ToList();
        }

        public IReadOnlyList<TEntity> RetornaTodosRegistros()
        {
            return context.Set<TEntity>().ToList();
        }

    }
}
