﻿using System.Collections.Generic;
using Texo.Teste.Domain.Entities;
using Texo.Teste.Service.Interface.DTOs;

namespace Texo.Teste.Service.Interface.Mappers
{
    public interface IIndicadosMapper
    {
        VencedoresPorAnoDTO MapToDto(IndicadoPiorFilme indicadoMapear);
        AnosMaisUmVencedorDTO MapToDto(List<AnosInfoDTO> anosMapear);
        EstudiosDTO MapToDto(List<InfoEstudioQtdPremiosDTO> infosEstudiosMapear);
        IntervalosPremiosDTO MapToDto(MaiorIntervaloDTO maiorIntervalMapear, MenorIntervaloDTO menorIntervalMapear);
    }
}
