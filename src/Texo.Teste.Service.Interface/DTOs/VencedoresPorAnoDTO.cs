﻿using System.Collections.Generic;

namespace Texo.Teste.Service.Interface.DTOs
{
    public class VencedoresPorAnoDTO
    {
        public int Id { get; set; }
        public int Year { get; set; }
        public string Title { get; set; }
        public List<string> Studios { get; set; }
        public List<string> Producers { get; set; }
        public bool Winner { get; set; }
    }
}
