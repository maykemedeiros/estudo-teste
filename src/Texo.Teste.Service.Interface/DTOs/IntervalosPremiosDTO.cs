﻿namespace Texo.Teste.Service.Interface.DTOs
{
    public class IntervalosPremiosDTO
    {
        public MenorIntervaloDTO Min { get; set; }
        public MaiorIntervaloDTO Max { get; set; }
    }
}
