﻿using System.Collections.Generic;

namespace Texo.Teste.Service.Interface.DTOs
{
    public class AnosMaisUmVencedorDTO
    {
        public List<AnosInfoDTO> Years { get; set; }

        public AnosMaisUmVencedorDTO()
        {
            Years = new List<AnosInfoDTO>();
        }
    }
}
