﻿namespace Texo.Teste.Service.Interface.DTOs
{
    public class IntervaloBasePremioDTO
    {
        public string Producer { get; set; }
        public int Interval { get; set; }
        public int PreviousWin { get; set; }
        public int FollowingWin { get; set; }
    }
}
