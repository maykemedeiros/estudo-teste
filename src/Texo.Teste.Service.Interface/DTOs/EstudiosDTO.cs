﻿using System.Collections.Generic;

namespace Texo.Teste.Service.Interface.DTOs
{
    public class EstudiosDTO
    {
        public List<InfoEstudioQtdPremiosDTO> Studios { get; set; }

        public EstudiosDTO()
        {
            Studios = new List<InfoEstudioQtdPremiosDTO>();
        }
    }
}
