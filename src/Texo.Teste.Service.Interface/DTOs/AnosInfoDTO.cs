﻿namespace Texo.Teste.Service.Interface.DTOs
{
    public class AnosInfoDTO
    {
        public int Year { get; set; }
        public int WinnerCount { get; set; }
    }
}
