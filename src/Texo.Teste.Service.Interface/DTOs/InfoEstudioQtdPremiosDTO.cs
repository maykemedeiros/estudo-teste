﻿namespace Texo.Teste.Service.Interface.DTOs
{
    public class InfoEstudioQtdPremiosDTO
    {
        public string Name { get; set; }
        public int WinCount { get; set; }
    }
}
