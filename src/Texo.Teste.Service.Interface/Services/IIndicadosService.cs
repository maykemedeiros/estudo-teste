﻿using System.Collections.Generic;
using Texo.Teste.Service.Interface.DTOs;

namespace Texo.Teste.Service.Interface.Services
{
    public interface IIndicadosService
    {
        bool Exclui(string nomeFilme);
        IReadOnlyList<VencedoresPorAnoDTO> ObterVencedoresEm(string ano);
        AnosMaisUmVencedorDTO ObterAnosComMaisDeUmVencedor();
        EstudiosDTO ObterEstudiosOrdenadoQtdPremio();
        IntervalosPremiosDTO GetProdutorMaiorIntervalosPremiosDoisPremiosMaisRápido();
    }
}
