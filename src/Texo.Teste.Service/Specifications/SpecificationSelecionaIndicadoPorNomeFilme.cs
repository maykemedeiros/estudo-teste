﻿using System;
using Texo.Teste.Domain.Entities;
using Texo.Teste.Repository.Interface.Specifications;

namespace Texo.Teste.Service.Specifications
{
    public class SpecificationSelecionaIndicadoPorNomeFilme : QuerySpecification<IndicadoPiorFilme>
    {
        public SpecificationSelecionaIndicadoPorNomeFilme(string nomeFile)
            : base(e => e.Titulo.Equals(nomeFile, StringComparison.CurrentCultureIgnoreCase))
        {
            AdicionarInclude(r => r.Produtores);
            AdicionarInclude(r => r.Estudios);
        }
    }
}
