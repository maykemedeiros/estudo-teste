﻿using System;
using Texo.Teste.Domain.Entities;
using Texo.Teste.Repository.Interface.Specifications;

namespace Texo.Teste.Service.Specifications
{
    class SpecificationGetAllVencedores : QuerySpecification<IndicadoPiorFilme>
    {
        public SpecificationGetAllVencedores()
            : base(e => e.Venceu)
        {
            AdicionarInclude(r => r.Produtores);
            AdicionarInclude(r => r.Estudios);
        }
    }
}
