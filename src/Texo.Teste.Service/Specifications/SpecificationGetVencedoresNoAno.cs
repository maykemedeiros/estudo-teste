﻿using Texo.Teste.Domain.Entities;
using Texo.Teste.Repository.Interface.Specifications;

namespace Texo.Teste.Service.Specifications
{
    public class SpecificationGetVencedoresNoAno : QuerySpecification<IndicadoPiorFilme>
    {
        public SpecificationGetVencedoresNoAno(string ano)
            : base(e => e.Ano.Equals(ano) && e.Venceu)
        {
            AdicionarInclude(r => r.Produtores);
            AdicionarInclude(r => r.Estudios);
        }
    }
}
