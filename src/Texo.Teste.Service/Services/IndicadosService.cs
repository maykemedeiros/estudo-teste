﻿using System;
using System.Collections.Generic;
using System.Linq;
using Texo.Teste.Domain.Entities;
using Texo.Teste.Domain.ValueObjects;
using Texo.Teste.Repository.Interface;
using Texo.Teste.Repository.Interface.Interfaces;
using Texo.Teste.Service.Interface.DTOs;
using Texo.Teste.Service.Interface.Mappers;
using Texo.Teste.Service.Interface.Services;
using Texo.Teste.Service.Specifications;

namespace Texo.Teste.Service.Services
{
    public class IndicadosService : IIndicadosService
    {
        private readonly IUnitOfWork unitOfWork;
        private readonly IBaseRepository<IndicadoPiorFilme> indicadoRepositorio;
        private readonly IIndicadosMapper indicadosMapper;

        public IndicadosService(IUnitOfWork unitOfWork,
            IIndicadosMapper indicadosMapper)
        {
            this.unitOfWork = unitOfWork;
            this.indicadosMapper = indicadosMapper;
            indicadoRepositorio = unitOfWork.RetornaRepositorioDe<IndicadoPiorFilme>();
        }

        public bool Exclui(string nomeFilme)
        {
            IndicadoPiorFilme indicadoPiorFilme = PesquisaIndicadoPor(nomeFilme);
            if (PodeExcluir(indicadoPiorFilme))
            {
                return indicadoRepositorio.Exclui(indicadoPiorFilme);
            }

            return false;
        }

        public IReadOnlyList<VencedoresPorAnoDTO> ObterVencedoresEm(string ano)
        {
            return indicadoRepositorio
                .PesquisaPor(new SpecificationGetVencedoresNoAno(ano))
                .Select(i => indicadosMapper.MapToDto(i))
                .ToList();
        }

        public AnosMaisUmVencedorDTO ObterAnosComMaisDeUmVencedor()
        {
            var vencedores = indicadoRepositorio
                .PesquisaPor(new SpecificationGetAllVencedores());

            var infoDTO = vencedores
                .GroupBy(i => i.Ano)
                .Where(i => i.Count(o => o.Venceu) > 1)
                .Select(i => new AnosInfoDTO { Year = int.Parse(i.Key), WinnerCount = i.Count(o => o.Venceu) })
                .ToList();

            return indicadosMapper.MapToDto(infoDTO);
        }

        public EstudiosDTO ObterEstudiosOrdenadoQtdPremio()
        {
            var vencedores = indicadoRepositorio
                .PesquisaPor(new SpecificationGetAllVencedores());

            var infoEstudios = vencedores.SelectMany(ev => ev.Estudios.Select(e => e.Nome))
                .GroupBy(ev => ev)
                .Select(i => new InfoEstudioQtdPremiosDTO { Name = i.Key, WinCount = i.Count() })
                .ToList();

            return indicadosMapper.MapToDto(infoEstudios);
        }

        public IntervalosPremiosDTO GetProdutorMaiorIntervalosPremiosDoisPremiosMaisRápido()
        {
            var vencedores = indicadoRepositorio
                .PesquisaPor(new SpecificationGetAllVencedores());

            var nomesProdutores = vencedores.SelectMany(v => v.Produtores.Select(p => p.Nome));
            var produtoresEAnos = new List<ProdutorVencedorPorAnos>();

            foreach (string produtor in nomesProdutores)
            {
                var anos = vencedores
                    .Where(v => v.Produtores
                            .Any(p => p.Nome.Equals(produtor,
                                                    StringComparison.CurrentCultureIgnoreCase)))
                    .Select(v => int.Parse(v.Ano));

                produtoresEAnos.Add(new ProdutorVencedorPorAnos(produtor, anos.ToList()));
            }

            var listaProdutoresAnosTratada = produtoresEAnos.Where(p => p.AnosVencidos.Count() > 1).ToList();

            MaiorIntervaloDTO maiorIntervalo = IdentificaMaiorIntervaloPremios(listaProdutoresAnosTratada);
            MenorIntervaloDTO menorIntervalo = IdentificaMenorIntervaloPremios(listaProdutoresAnosTratada);

            return indicadosMapper.MapToDto(maiorIntervalo, menorIntervalo);
        }

        private MenorIntervaloDTO IdentificaMenorIntervaloPremios(List<ProdutorVencedorPorAnos> produtoresEAnos)
        {
            var nomeProdutor = string.Empty;
            var maiorAno = 0;
            var menorAno = 0;
            var diferencaAnos = 99999;

            foreach (var produtorAno in produtoresEAnos)
            {
                var menorAnoLista = produtorAno.AnosVencidos.Min();
                var maiorAnoLista = produtorAno.AnosVencidos.Max();
                var diferencaAnosLista = maiorAnoLista - menorAnoLista;

                if (diferencaAnosLista < diferencaAnos)
                {
                    nomeProdutor = produtorAno.Produtor;
                    menorAno = menorAnoLista;
                    maiorAno = maiorAnoLista;
                    diferencaAnos = diferencaAnosLista;
                }
            }

            return
                new MenorIntervaloDTO()
                {
                    Producer = nomeProdutor,
                    Interval = diferencaAnos,
                    FollowingWin = maiorAno,
                    PreviousWin = menorAno
                };
        }

        private MaiorIntervaloDTO IdentificaMaiorIntervaloPremios(List<ProdutorVencedorPorAnos> produtoresEAnos)
        {
            var nomeProdutor = string.Empty;
            var maiorAno = 0;
            var menorAno = 0;
            var diferencaAnos = 0;

            foreach (var produtorAno in produtoresEAnos)
            {
                var menorAnoLista = produtorAno.AnosVencidos.Min();
                var maiorAnoLista = produtorAno.AnosVencidos.Max();
                var diferencaAnosLista = maiorAnoLista - menorAnoLista;
    
                if (diferencaAnosLista > diferencaAnos)
                {
                    nomeProdutor = produtorAno.Produtor;
                    menorAno = menorAnoLista;
                    maiorAno = maiorAnoLista;
                    diferencaAnos = diferencaAnosLista;
                }
            }

            return 
                new MaiorIntervaloDTO() { Producer = nomeProdutor,
                                            Interval = diferencaAnos,
                                            FollowingWin = maiorAno,
                                            PreviousWin = menorAno };
        }

        private bool PodeExcluir(IndicadoPiorFilme indicado)
        {
            return indicado != default(IndicadoPiorFilme) && !indicado.Venceu;
        }

        private IndicadoPiorFilme PesquisaIndicadoPor(string nomeFilme)
        {
            return indicadoRepositorio
                .PesquisaPor(new SpecificationSelecionaIndicadoPorNomeFilme(nomeFilme))
                .FirstOrDefault();
        }
    }
}
