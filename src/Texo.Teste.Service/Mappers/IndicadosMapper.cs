﻿using System;
using System.Collections.Generic;
using System.Linq;
using Texo.Teste.Domain.Entities;
using Texo.Teste.Service.Interface.DTOs;
using Texo.Teste.Service.Interface.Mappers;

namespace Texo.Teste.Service.Mappers
{
    public class IndicadosMapper : IIndicadosMapper
    {
        public VencedoresPorAnoDTO MapToDto(IndicadoPiorFilme indicadoMapear) =>
            new VencedoresPorAnoDTO
            {
                Id = indicadoMapear.Id,
                Title = indicadoMapear.Titulo,
                Year = Convert.ToInt32(indicadoMapear.Ano),
                Producers = indicadoMapear.Produtores
                    .Select(p => p.Nome).ToList(),
                Studios = indicadoMapear.Estudios
                    .Select(s => s.Nome).ToList(),
                Winner = indicadoMapear.Venceu
            };

        public AnosMaisUmVencedorDTO MapToDto(List<AnosInfoDTO> anosMapear)
        {
            var dto = new AnosMaisUmVencedorDTO();
            dto.Years.AddRange(anosMapear);

            return dto;
        }

        public EstudiosDTO MapToDto(List<InfoEstudioQtdPremiosDTO> infosEstudiosMapear)
        {
            var dto = new EstudiosDTO();
            dto.Studios.AddRange(infosEstudiosMapear.OrderByDescending(s => s.WinCount));

            return dto;
        }

        public IntervalosPremiosDTO MapToDto(MaiorIntervaloDTO maiorIntervalMapear, MenorIntervaloDTO menorIntervalMapear)
        {
            var dto = new IntervalosPremiosDTO();
            dto.Max = maiorIntervalMapear;
            dto.Min = menorIntervalMapear;

            return dto;
        }
    }
}
