﻿using System.Text.RegularExpressions;

namespace Texo.Teste.Domain.Extensions
{
    public static class StringExtensions
    {
        public static bool OnlyNumbers(this string valor)
        {
            Regex reg = new Regex("^[0-9]+$");

            return reg.Match(valor).Success;
        }
    }
}
