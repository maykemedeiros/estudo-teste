﻿using System.Collections.Generic;

namespace Texo.Teste.Domain.ValueObjects
{
    public class ProdutorVencedorPorAnos
    {
        public string Produtor { get; private set; }
        public List<int> AnosVencidos { get; private set; }

        public ProdutorVencedorPorAnos(string produtor, List<int> anosVencidos)
        {
            Produtor = produtor;
            AnosVencidos = anosVencidos;
        }
    }
}
