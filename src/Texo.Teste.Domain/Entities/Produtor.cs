﻿using System.ComponentModel.DataAnnotations;

namespace Texo.Teste.Domain.Entities
{
    public class Produtor
    {
        [Key]
        public int Id { get; set; }
        public string Nome { get; set; }
    }
}
