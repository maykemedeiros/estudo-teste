﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Texo.Teste.Domain.Entities
{
    public class IndicadoPiorFilme : BaseEntity
    {
        [Key]
        public int Id { get; set; }
        public string Ano { get; set; }
        public string Titulo { get; set; }
        public List<Estudio> Estudios { get; set; }
        public List<Produtor> Produtores { get; set; }
        public bool Venceu { get; set; }
    }
}
