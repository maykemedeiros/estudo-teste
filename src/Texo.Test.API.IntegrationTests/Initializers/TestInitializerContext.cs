﻿using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using Microsoft.Extensions.Configuration;
using System.Net.Http;
using Texo.Teste.API;

namespace Texo.Test.API.IntegrationTests.Initializers
{
    public class TestInitializerContext
    {
        public HttpClient Client { get; set; }
        private TestServer server;

        public TestInitializerContext()
        {
            SetupClient();
        }

        private void SetupClient()
        {
            server = new TestServer(new WebHostBuilder()
                .UseEnvironment("Development")
                .UseConfiguration(new ConfigurationBuilder()
                    .AddJsonFile("appsettings.json")
                    .Build()
                )
                .UseStartup<Startup>());

            Client = server.CreateClient();
        }
    }
}
