﻿using FluentAssertions;
using System.Net;
using System.Threading.Tasks;
using Texo.Test.API.IntegrationTests.Initializers;
using Xunit;

namespace Texo.Test.API.IntegrationTests.Scenarios
{
    public class VencedoresControllerTests
    {
        private readonly TestInitializerContext testInitializerContext;
        private const string ENDPOINT_CONTROLLER = "/api/piorfilme/vencedores/";

        public VencedoresControllerTests()
        {
            testInitializerContext = new TestInitializerContext();
        }

        [Fact]
        public async Task Vencedores_GetVencedoresEm_OkResponse()
        {
            var response = await testInitializerContext
                .Client.GetAsync($"{ENDPOINT_CONTROLLER}ano/1980");
            response.EnsureSuccessStatusCode();
            response.StatusCode.Should().Be(HttpStatusCode.OK);
        }

        [Fact]
        public async Task Vencedores_GetVencedoresEm_BadRequestResponse()
        {
            var response = await testInitializerContext
                .Client.GetAsync($"{ENDPOINT_CONTROLLER}ano/testeerroaqui");
            response.StatusCode.Should().Be(HttpStatusCode.BadRequest);
        }

        [Fact]
        public async Task Vencedores_GetAnosComMaisDeUmVencedor_OkResponse()
        {
            var response = await testInitializerContext
                .Client.GetAsync($"{ENDPOINT_CONTROLLER}ano/anos_com_mais_de_um");
            response.EnsureSuccessStatusCode();
            response.StatusCode.Should().Be(HttpStatusCode.OK);
        }

        [Fact]
        public async Task Vencedores_EstudioOrdenadoPorQtdPremiacoes_OkResponse()
        {
            var response = await testInitializerContext
                .Client.GetAsync($"{ENDPOINT_CONTROLLER}estudios/ordenado_por_premiacoes");
            response.EnsureSuccessStatusCode();
            response.StatusCode.Should().Be(HttpStatusCode.OK);
        }

        [Fact]
        public async Task Vencedores_GetProdutorMaiorEMenorIntervalosPremios_OkResponse()
        {
            var response = await testInitializerContext
                .Client.GetAsync($"{ENDPOINT_CONTROLLER}produtor/maior_menor_intervalo_premios");
            response.EnsureSuccessStatusCode();
            response.StatusCode.Should().Be(HttpStatusCode.OK);
        }

        [Fact]
        public async Task Values_AllMethods_CorrectContentType_ReturnsJSON()
        {
            string[] allEndpoints = { "ano/1980",
                                    "ano/anos_com_mais_de_um",
                                    "estudios/ordenado_por_premiacoes",
                                    "produtor/maior_menor_intervalo_premios"
                                    };

            for (int i = 0; i < allEndpoints.Length; i++)
            {
                var response = await testInitializerContext.Client.GetAsync($"{ENDPOINT_CONTROLLER}{allEndpoints[i]}");
                response.EnsureSuccessStatusCode();
                response.Content.Headers.ContentType.ToString().Should().Be("application/json; charset=utf-8");
            }
        }
    }
}
