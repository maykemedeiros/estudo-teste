﻿using FluentAssertions;
using System.Net;
using System.Threading.Tasks;
using Texo.Test.API.IntegrationTests.Initializers;
using Xunit;

namespace Texo.Test.API.IntegrationTests.Scenarios
{
    public class FilmesControllerTests
    {
        private readonly TestInitializerContext testInitializerContext;
        private const string ENDPOINT_CONTROLLER = "/api/piorfilme/filmes/";

        public FilmesControllerTests()
        {
            testInitializerContext = new TestInitializerContext();
        }

        [Fact]
        public async Task Filmes_DeleteExcluirFilme_OkResponse()
        {
            var response = await testInitializerContext
                .Client.DeleteAsync($"{ENDPOINT_CONTROLLER}excluir/Cruising");
            response.EnsureSuccessStatusCode();
            response.StatusCode.Should().Be(HttpStatusCode.OK);
        }

        [Fact]
        public async Task Filmes_DeleteExcluirFilme_BadResponse()
        {
            var response = await testInitializerContext
                .Client.DeleteAsync($"{ENDPOINT_CONTROLLER}excluir/Can't Stop the Music");
            response.StatusCode.Should().Be(HttpStatusCode.BadRequest);
        }

        [Fact]
        public async Task Filmes_DeleteExcluirFilme_CorrectContentType()
        {
            var response = await testInitializerContext
                .Client.DeleteAsync($"{ENDPOINT_CONTROLLER}excluir/The Formula");
            response.EnsureSuccessStatusCode();
            response.Content.Headers.ContentType.ToString().Should().Be("text/plain; charset=utf-8");
        }
    }
}
