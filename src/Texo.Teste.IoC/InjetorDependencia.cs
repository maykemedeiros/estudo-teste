﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Texo.Teste.Repository;
using Texo.Teste.Repository.Interface;
using Texo.Teste.Service.Interface.Mappers;
using Texo.Teste.Service.Interface.Services;
using Texo.Teste.Service.Mappers;
using Texo.Teste.Service.Services;

namespace Texo.Teste.IoC
{
    public class InjetorDependencia
    {
        public static void Register(IServiceCollection services)
        {
            RegistraUnitOfWork(services);
            RegistraServices(services);
            RegistraMappers(services);
        }

        private static void RegistraUnitOfWork(IServiceCollection services) =>
            services.TryAddScoped<IUnitOfWork, UnitOfWork>();

        private static void RegistraServices(IServiceCollection services) =>
            services.TryAddScoped<IIndicadosService, IndicadosService>();

        private static void RegistraMappers(IServiceCollection services)
        {
            services.TryAddScoped<IIndicadosMapper, IndicadosMapper> ();
        }

    }
}
