
## Projeto de teste para Texo IT

API REST com nível 2 de modelo de maturidade de Richardson. 

OBS: Na exclusão de registro, foi utilizado o verbo DELETE, pois, semanticamente e segundo a RFC 7231, esse verbo remove o conteúdo relacionado ao resource. Não foi utilizado nem POST e nem PUT, no método de exclusão, para manter a semântica, pois a exclusão do conteúdo não está relacionado à mudança de estado do conteúdo por não ter sido utilizado exclusão lógica.

OBS2: É necessário ter o SDK do .NET CORE 2.1 instalado para rodar o projeto. O SDK está disponível em: https://www.microsoft.com/net/download

** Como rodar o projeto

1. Clonar o repositório
2. Alterar o appsettings do assembly Texo.Teste.API, modificando o valor do campo Path para onde o arquivo movielist.csv ficará.
3. Abrir a solution no Visual Studio 2017
4. Selecionar o Texo.Teste.API como Start up Project e dar play 
5. O projeto abrirá uma página do Swagger, onde contém todos os endpoints e a documentação de todos os métodos da API, com suas respectivas entradas.
6. Pelo Swagger é possível testar todos os endpoints, os quais retornarão seus respectivos resultados da mesma forma como um client REST normal, como POSTMAN 
6. Para os testes de integração automatizados, utilizar a opção de testes do Visual Studio 2017 no menu superior, ao lado de Tools (Ferramentas, para Visual Studio em pt-BR) 


---

